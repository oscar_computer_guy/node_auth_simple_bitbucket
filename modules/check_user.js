var log = require("./log.js");
var User = require("../models/user.model");
var jwt = require("jsonwebtoken");
var compare = require("./pass_comp");

const userok = route => {
  return (req, res, next) => {
    const { username, password, ip } = req.body;

    if (!username || !password) {
      res.status(400).json({ message: "username or password missing" });
      return;
    }

    const query = { username: username };
    let message = "";
    User.find(query)
      .exec()
      .then(doc => {
        switch (doc.length) {
          case 0:
            message = {
              script: "check_user",
              errorMessage: username + " does not exist"
            };
            log.setMessage(message);
            res
              .status(400)
              .json({ message: "User " + username + " not found" });
            break;

          case 1:
            compare.checkit(password, doc[0].password, (mess, result) => {
              if (result) {
                message = {
                  script: "check_user",
                  errorMessage: "User " + username + " login"
                };
                log.setMessage(message);
                if (route === "login") {
                  res.status(200).json({
                    username: username,
                    role: doc[0].role,
                    jwt: jwt.sign(
                      {
                        id: 1,
                        login: username,
                        role: doc[0].role
                      },
                      process.env.JWT_SECRET,
                      { expiresIn: 600 }
                    )
                  });
                  next();
                }
                if (route === "newuser") {
                  next();
                }
              } else {
                message = {
                  script: "check_user",
                  errorMessage: username + " " + mess
                };
                log.setMessage(message);

                res.status(400).json({ message: mess });
              }
            });
            break;
          default:
            message = {
              script: "check_user",
              errorMessage: "More than one user for " + username
            };
            log.setMessage(message);
            res
              .status(400)
              .json({ message: "More than one user for " + username });
            break;
        }
      })
      .catch(err => {
        res.status(400).json({ message: "Error querying database" });
      });
  };
};

module.exports.userok = userok;
