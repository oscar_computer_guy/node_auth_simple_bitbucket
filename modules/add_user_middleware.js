const useradd = require("./add_user");

const userAdd = (req, res, next) => {
  useradd.useradd(
    req.body.addusername,
    req.body.addpassword,
    req.body.addrole,
    function(message, response) {
      if (response) {
        res.status(200).json({
          message: message
        });
        next();
      } else {
        res.status(400).json({ message: message });
      }
    }
  );
};

module.exports.userAdd = userAdd;
