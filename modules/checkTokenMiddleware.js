const jwt = require("jsonwebtoken");

const checkTokenMiddleware = (req, res, next) => {
  if (
    req.hasOwnProperty("headers") &&
    req.headers.hasOwnProperty("authorization")
  ) {
    let token = req.headers.authorization;
    if (token === "null") {
      res.status(401).json({ message: "Bad Token" });
    }
    jwt.verify(token, process.env.JWT_SECRET, function(error, decoded) {
      if (error) {
        res.status(401).json({ message: "Bad Token" });
      } else {
        res.status(200).json({ message: true });
        next();
      }
    });
  } else {
    res.status(401).json({ message: "No Token" });
  }
};

module.exports.checkTokenMiddleware = checkTokenMiddleware;
