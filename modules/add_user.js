const validate = require("./check_name_password").validate;
const insert_user = require("./insert_user").insert_user;

const useradd = (username, password, role, callback) => {
  validate(username, password, function(mess, resp) {
    if (resp) {
      const bcrypt = require("bcryptjs");
      const salt = bcrypt.genSaltSync(10);
      const hashpassword = bcrypt.hashSync(password, salt);
      insert_user(username, hashpassword, role, function(mess, resp) {
        callback(mess, resp);
      });
    } else {
      callback(mess, resp);
    }
  });
};

module.exports.useradd = useradd;
