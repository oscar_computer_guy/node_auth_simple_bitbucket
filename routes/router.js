const express = require("express");

const check_user = require("../modules/check_user").userok;
const connect = require("../modules/dbConnect");

const userAddMiddleware = require("../modules/add_user_middleware").userAdd;
const checkTokenMiddleware = require("../modules/checkTokenMiddleware")
  .checkTokenMiddleware;

const router = express.Router();

router.get("/", function(req, res) {
  res.json({ message: "Welcome to the user validation api" });
});

router.post("/checkToken", checkTokenMiddleware);

router.use(connect.checkCon);

router.post("/", check_user("login"));

router.post("/newuser", [check_user("newuser"), userAddMiddleware]);

module.exports = router;
